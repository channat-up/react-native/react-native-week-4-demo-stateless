import React from 'react'
import {View, Text, StyleSheet} from 'react-native'

const Card = (props) => {

    console.log("==> ", props);

    const { person } = props;
    const { textStyle, containerStyle } = styles;

    return (
        <View style={containerStyle}>
            <Text style={textStyle}>Name: { person.name }</Text>
            <Text>Age: { person.age } </Text>
            <Text>Gender: { person.gender } </Text>
        </View>
    )
}


const styles = StyleSheet.create({
    textStyle: {
        fontSize: 15,
        fontWeight: 'bold'
    },
    containerStyle: {

    }

})

export { Card }
