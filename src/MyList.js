import React, {Component} from 'react'
import { View, Text, FlatList, StyleSheet, ActivityIndicator } from 'react-native'


class MyList extends Component {

    state = {
        data: [
            {key: 'One'},
            {key: 'Two'},
            {key: 'Two'},
            {key: 'Two'},
            {key: 'Two'},
            {key: 'Two'},
            {key: 'Two'},
            {key: 'Two'}
        ],
        students: [
            {key: 'Mr. A'},
            {key: 'Mr. B'},
            {key: 'Mr. C'},
            {key: 'Mr. D'},
            {key: 'Mr. E'},
            {key: 'Mr. F'},
        ]
    }

    render() {
        return (
            <View style={styles.containerStyle}>
                <FlatList
                    data={this.state.data}
                    renderItem={(item) => (
                        <Text style={styles.textStyle}>{item.key}</Text>
                    )}
                />

                <FlatList
                    data={this.state.students}
                    horizontal={true}
                    renderItem={({item}) => <Text>{ item.key }</Text>}
                />

                <ActivityIndicator
                    size={"large"}
                    color={'#ff00ff'}
                />
            </View>
        )
    }
}


const styles = StyleSheet.create({
    containerStyle: {
        marginTop: 30
    },
    textStyle: {
        fontWeight: 'bold',
        fontSize: 25
    }
})




export default MyList