/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import {Card} from './src/Card'


type Props = {};
export default class App extends Component<Props> {

  state = {
    students: [
      {
        name: 'Channy',
        age: 30,
        gender: 'M'
      },
      {
        name: 'Pesiey',
        age: 21,
        gender: 'F'
      }
    ]

  }

  render() {
    return (
      <View style={styles.container}>
        { this.state.students.forEach(student => {
          console.log("=> ", student.name)
        })}

      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginTop: 50,

  }
});
